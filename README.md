# malichimp-api-java-wraper

This library provides acess to MailChimp API v3.0 methods from Java.

It is written in Java language and be used on any language which runs un JVM.

# Supported Mailchimp Methods:
  - Members
  - Reports
  - Campaigns
  - Lists
  