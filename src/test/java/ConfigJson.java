import com.fasterxml.jackson.annotation.JsonProperty;

public class ConfigJson {

    private String url;
    private String apiKey;
    private String user;

    @JsonProperty("url")
    public String getURL() { return url; }
    @JsonProperty("url")
    public void setURL(String value) { this.url = value; }

    @JsonProperty("api_key")
    public String getAPIKey() { return apiKey; }
    @JsonProperty("api_key")
    public void setAPIKey(String value) { this.apiKey = value; }

    @JsonProperty("user")
    public String getUser() { return user; }
    @JsonProperty("user")
    public void setUser(String value) { this.user = value; }

}
