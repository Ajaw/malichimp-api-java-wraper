import Client.MailChimpClientBuilder;
import Client.MailchimpClient;
import Methods.Campaings.MailchimpCampaingsRequests;
import Methods.Campaings.Reponse.Campaign;
import Methods.Campaings.Reponse.CampaignsResponse;
import Methods.Campaings.Request.CampaingsRequest;
import exception.RequestFailedException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class TestCampaignsRequest {


    private MailchimpClient mailchimpClient;

    private String createdCampaingId;

    @Before
    public void before() {
        MailChimpClientBuilder builder = new MailChimpClientBuilder();
        ResourcesConfig resourcesConfig =  new ResourcesConfig();
        ConfigJson configJson = resourcesConfig.getConfigJson();
        builder.withApiKey(configJson.getAPIKey());
        builder.withUrl(configJson.getURL());
        builder.withUserName(configJson.getUser());
        mailchimpClient = builder.build();
    }

   @Test
    public void testCampaingCreation(){
       MailchimpCampaingsRequests mailchimpCampaingsRequests = new MailchimpCampaingsRequests(mailchimpClient);

       CampaingsRequest campaingsRequest = new CampaingsRequest();
        campaingsRequest.setType("regular");

       try {
           Campaign response = mailchimpCampaingsRequests.createCampaing(campaingsRequest);
           createdCampaingId = response.getID();
           Assert.assertNotNull(createdCampaingId);
           this.getCampaigns(mailchimpCampaingsRequests);
       } catch (RequestFailedException e) {
           e.printStackTrace();
           Assert.fail();
       }

   }


    private void getCampaigns(MailchimpCampaingsRequests mailchimpCampaingsRequests) throws RequestFailedException {
        CampaignsResponse resposne = mailchimpCampaingsRequests.getCampaings();
        List<Campaign> campaignsResponseList = Arrays.asList( resposne.getCampaigns());
        boolean foundCampaign = false;
        for (Campaign campaign : campaignsResponseList) {
            if (campaign.getID().equalsIgnoreCase(this.createdCampaingId)) {
                foundCampaign = true;
                break;
            }
        }
        mailchimpCampaingsRequests.deleteCampaign(this.createdCampaingId);
        CampaignsResponse campaignsResponse = mailchimpCampaingsRequests.getCampaings();
        Assert.assertEquals(campaignsResponseList.size()-1,campaignsResponse.getCampaigns().length);
   }
}
