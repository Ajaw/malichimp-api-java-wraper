import Client.MailChimpClientBuilder;
import Client.MailchimpClient;
import Methods.Lists.MailChimpListRequests;
import Methods.Lists.Members.MailChimpListMembersRequests;
import Methods.Lists.Members.Request.MembersRequest;
import Methods.Lists.Members.Response.Member;
import Methods.Lists.Members.Response.MembersResponse;
import Methods.Lists.Request.CampaignDefaults;
import Methods.Lists.Request.Contact;
import Methods.Lists.Request.ListsRequest;
import Methods.Lists.Response.List;
import Methods.Lists.Response.ListsResponse;
import exception.RequestFailedException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

public class TestListsMembersRequest {


    private MailchimpClient mailchimpClient;

    private String listId;

    @Before
    public void before() {
        MailChimpClientBuilder builder = new MailChimpClientBuilder();
        ResourcesConfig resourcesConfig =  new ResourcesConfig();
        ConfigJson configJson = resourcesConfig.getConfigJson();
        builder.withApiKey(configJson.getAPIKey());
        builder.withUrl(configJson.getURL());
        builder.withUserName(configJson.getUser());
        mailchimpClient = builder.build();
    }



    @Test
    public void testListsMembersRequests() {

        MailChimpListRequests mailChimpListRequests = new MailChimpListRequests(mailchimpClient);

        this.createMalingList();

        Assert.assertNotNull(listId);

        MailChimpListMembersRequests request = new MailChimpListMembersRequests(mailchimpClient);

        MembersRequest membersRequest = new MembersRequest();
        membersRequest.setEmailAddress("test-pronto@mail-apps.com");
        membersRequest.setStatus("subscribed");

        try {
           Member member = request.createMember(this.listId,membersRequest);
            Assert.assertNotNull(member.getID());
            MembersResponse membersResposne = request.getMembers(this.listId);

            request.deleteMember(this.listId,member.getID());

            MembersResponse response =request.getMembers(this.listId);

            Assert.assertEquals(membersResposne.getMembers().length-1,response.getMembers().length);

            mailChimpListRequests.deleteList(this.listId);
        } catch (RequestFailedException e) {
            e.printStackTrace();
        }


    }

    private void createMalingList(){
        MailChimpListRequests mailChimpListRequests = new MailChimpListRequests(mailchimpClient);

        TestListsRequest testListsRequest = new TestListsRequest();

        try {
            List list = mailChimpListRequests.createList(testListsRequest.getListsRequest());
            this.listId = list.getID();
        } catch (RequestFailedException e) {
            e.printStackTrace();
        }
    }

    private void testListsDeleteRequest(MailChimpListRequests mailChimpListRequests) {
        try {
            ListsResponse listsResponse = mailChimpListRequests.getLists();
            for (List list : listsResponse.getLists()) {
                if (list.getName().equalsIgnoreCase("test list")) {
                    mailChimpListRequests.deleteList(list.getID());
                }
            }
             listsResponse = mailChimpListRequests.getLists();
            Assert.assertEquals(listsResponse.getLists().length-1, listsResponse.getLists().length);
        } catch (RequestFailedException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }


}
