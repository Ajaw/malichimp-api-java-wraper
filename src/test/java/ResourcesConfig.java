import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.corba.se.impl.encoding.ByteBufferWithInfo;
import org.apache.commons.io.IOUtils;

import java.io.*;

public class ResourcesConfig {

    public ConfigJson getConfigJson() {
        return configJson;
    }

    private ConfigJson configJson;

    ResourcesConfig(){
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("config.json").getFile());

        ObjectMapper objectMapper = new ObjectMapper();
        try {
             configJson = objectMapper.readValue(file,ConfigJson.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }



}
