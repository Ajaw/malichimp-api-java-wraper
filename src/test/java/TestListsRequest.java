import Client.MailChimpClientBuilder;
import Client.MailchimpClient;
import Methods.Lists.MailChimpListRequests;
import Methods.Lists.Request.CampaignDefaults;
import Methods.Lists.Request.Contact;
import Methods.Lists.Request.ListsRequest;
import Methods.Lists.Response.List;
import Methods.Lists.Response.ListsResponse;
import exception.RequestFailedException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

public class TestListsRequest {


    private MailchimpClient mailchimpClient;

    @Before
    public void before() {
        MailChimpClientBuilder builder = new MailChimpClientBuilder();
        ResourcesConfig resourcesConfig =  new ResourcesConfig();
        ConfigJson configJson = resourcesConfig.getConfigJson();
        builder.withApiKey(configJson.getAPIKey());
        builder.withUrl(configJson.getURL());
        builder.withUserName(configJson.getUser());
        mailchimpClient = builder.build();
    }

    @Test
    public void testListsRequests() {

        MailChimpListRequests mailChimpListRequests = new MailChimpListRequests(mailchimpClient);
        ListsRequest listsRequest = getListsRequest();


        try {
            List list = (List) mailChimpListRequests.createList(listsRequest);
            Assert.assertNotNull(list.getID());
        } catch (RequestFailedException e) {
            e.printStackTrace();
            Assert.fail();
        }

    }

    public ListsRequest getListsRequest() {
        ListsRequest listsRequest = new ListsRequest();
        listsRequest.setEmailTypeOption(true);
        listsRequest.setName("test list");
        listsRequest.setPermissionReminder("true");
        Contact contact = new Contact();
        contact.setAddress1("test");
        contact.setAddress2("test");
        contact.setCity("test");
        contact.setPhone("7473147");
        contact.setCountry("PL");
        contact.setZip("43-190");
        contact.setState("Silesia");
        contact.setCompany("test company");
        listsRequest.setContact(contact);

        CampaignDefaults campaignDefaults = new CampaignDefaults();
        campaignDefaults.setFromEmail("test@emai.com");
        campaignDefaults.setFromName("test campaign");
        campaignDefaults.setLanguage("PL");
        campaignDefaults.setSubject("test subject");
        listsRequest.setCampaignDefaults(campaignDefaults);
        return listsRequest;
    }

    @Test
    public void testListsGetRequsest() {

        MailChimpListRequests mailChimpListRequests = new MailChimpListRequests(mailchimpClient);


        try {

            ListsResponse list = mailChimpListRequests.getLists();
            boolean foundList = false;
            for (List list1 : Arrays.asList(list.getLists())) {
                if (list1.getName().equalsIgnoreCase("test list")) {
                    foundList = true;
                }
            }
            Assert.assertTrue(foundList);
            System.out.print(list.getLists().length);
            testListsDeleteRequest(mailChimpListRequests);
        } catch (RequestFailedException e) {
            e.printStackTrace();
            Assert.fail();
        }

    }

    private void testListsDeleteRequest(MailChimpListRequests mailChimpListRequests) {
        try {
            ListsResponse listsResponse = mailChimpListRequests.getLists();
            for (List list : listsResponse.getLists()) {
                if (list.getName().equalsIgnoreCase("test list")) {
                    mailChimpListRequests.deleteList(list.getID());
                }
            }
             listsResponse = mailChimpListRequests.getLists();
            Assert.assertEquals(listsResponse.getLists().length-1, listsResponse.getLists().length);
        } catch (RequestFailedException e) {
            e.printStackTrace();
            Assert.fail();
        }
    }


}
