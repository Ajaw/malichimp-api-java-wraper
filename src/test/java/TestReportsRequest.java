import Client.MailChimpClientBuilder;
import Client.MailchimpClient;
import Methods.Campaings.MailchimpCampaingsRequests;
import Methods.Campaings.Reponse.Campaign;
import Methods.Campaings.Reponse.CampaignsResponse;
import Methods.Lists.MailChimpListRequests;
import Methods.Lists.Members.MailChimpListMembersRequests;
import Methods.Lists.Members.Request.MembersRequest;
import Methods.Lists.Members.Response.Member;
import Methods.Lists.Members.Response.MembersResponse;
import Methods.Lists.Response.List;
import Methods.Lists.Response.ListsResponse;
import Methods.Reports.EmailActivity.MailChimpEmailActivityRequests;
import Methods.Reports.EmailActivity.Response.EmailActivityResponse;
import Methods.Reports.Unsubscribed.MailChimpUnsubscribeRequests;
import Methods.Reports.Unsubscribed.Response.UnsubscribeResponse;
import exception.RequestFailedException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestReportsRequest {


    private MailchimpClient mailchimpClient;

    private String listId;

    @Before
    public void before() {
        MailChimpClientBuilder builder = new MailChimpClientBuilder();
        ResourcesConfig resourcesConfig =  new ResourcesConfig();
        ConfigJson configJson = resourcesConfig.getConfigJson();
        builder.withApiKey(configJson.getAPIKey());
        builder.withUrl(configJson.getURL());
        builder.withUserName(configJson.getUser());
        mailchimpClient = builder.build();
    }



    @Test
    public void testReportRequest() {

        MailChimpEmailActivityRequests mailChimpEmailActivityRequests = new MailChimpEmailActivityRequests(mailchimpClient);

        MailchimpCampaingsRequests mailChimpListRequests = new MailchimpCampaingsRequests(mailchimpClient);

        try {
            CampaignsResponse campaings = mailChimpListRequests.getCampaings();

            if (campaings.getCampaigns().length>0){
                Campaign campaign = campaings.getCampaigns()[0];
                EmailActivityResponse emailActivityResponse = mailChimpEmailActivityRequests.getEmailActivityResponse(campaign.getID());
                MailChimpUnsubscribeRequests mailChimpUnsubscribeRequests = new MailChimpUnsubscribeRequests(mailchimpClient);
                UnsubscribeResponse unsubscribeResponse = mailChimpUnsubscribeRequests.getUnsubscribeReport(campaign.getID());

                Assert.assertNotNull(unsubscribeResponse);
                Assert.assertNotNull(emailActivityResponse);


            }


        } catch (RequestFailedException e) {
            e.printStackTrace();
        }




    }




}
