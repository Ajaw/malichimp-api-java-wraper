package exception;


import Methods.MailchimpError;

public class RequestFailedException extends Throwable {

    private static final long serialVersionUID = 1L;
    private MailchimpError error;

    public RequestFailedException(String message, Throwable t) {
        super(message, t);
    }

    public RequestFailedException(String message) {
        super(message);
    }

    public RequestFailedException(String message, MailchimpError error) {
        super(message);
        this.error = error;
    }

    public MailchimpError getError() {
        return error;
    }
}
