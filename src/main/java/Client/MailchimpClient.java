package Client;

import Methods.BaseMailchimpResponse;
import Methods.BaseMalichimpRequest;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import exception.RequestFailedException;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public class MailchimpClient {


    private String apiKey;
    private String url;
    private String userName;
    private Logger logger = LoggerFactory.getLogger(MailchimpClient.class);
    private CloseableHttpClient httpClient;
    private ObjectMapper objectMapper;
    private Integer timeoutValue;

    public MailchimpClient(MailChimpClientBuilder builder) {
        this.setApiKey(builder.getApiKey());
        this.setUrl(builder.getUrl());
        this.userName = builder.getUserName();
        if (builder.getTimeoutValue() != null) {
            this.timeoutValue = builder.getTimeoutValue();
        } else {
            this.timeoutValue = 10000;
        }
        objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        createHttpClient();


    }


    private void setUrl(String url) {
        this.url = url;
    }


    private void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }


    private void createHttpClient() {
        HttpClientBuilder b = HttpClientBuilder.create();

        SSLContext sslContext = null;
        try {
            sslContext = new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy() {
                public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                    return true;
                }
            }).build();
        } catch (NoSuchAlgorithmException e) {
            logger.error(e.getMessage());
        } catch (KeyManagementException e) {
            logger.error(e.getMessage());
        } catch (KeyStoreException e) {
            logger.error(e.getMessage());
        }
        b.setSslcontext(sslContext);

        // don't check Hostnames, either.
        //      -- use SSLConnectionSocketFactory.getDefaultHostnameVerifier(), if you don't want to weaken
        HostnameVerifier hostnameVerifier = SSLConnectionSocketFactory.getDefaultHostnameVerifier();

        // here's the special part:
        //      -- need to create an SSL Socket Factory, to use our weakened "trust strategy";
        //      -- and create a Registry, to register it.
        //
        SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(sslContext, hostnameVerifier);
        Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create().register("http", PlainConnectionSocketFactory.getSocketFactory()).register("https", sslSocketFactory).build();

        // now, we create connection-manager using our Registry.
        //      -- allows multi-threaded use
        PoolingHttpClientConnectionManager connMgr = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
        b.setConnectionManager(connMgr);
        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(new AuthScope(null, -1),
                new UsernamePasswordCredentials("domin174", this.apiKey));
        // finally create the HttpClient using HttpClient factory methods and assign the ssl socket factory
        //   b.setDefaultCredentialsProvider(credentialsProvider);
        httpClient = b.build();
    }


    public BaseMailchimpResponse performGetRequest(String serviceMethod, Object responseClass) throws RequestFailedException {
        try {
            String finalUrl =this.url + serviceMethod;
            HttpGet httpGet = new HttpGet(finalUrl);
            String userpass = this.userName + ":" + this.apiKey;
            httpGet.addHeader("Authorization", "Basic " + new String(new Base64().encode(userpass.getBytes())));


            HttpResponse response = httpClient.execute(httpGet);
            RequestConfig requestConfig = RequestConfig.custom()
                    .setConnectTimeout(timeoutValue)
                    .setSocketTimeout(timeoutValue)
                    .setConnectionRequestTimeout(timeoutValue)
                    .build();
            httpGet.setConfig(requestConfig);

            BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));

            StringBuffer sb = new StringBuffer();
            String output;
            // System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                sb.append(output);
                // System.out.println(output);
            }
            ((CloseableHttpResponse) response).close();
            String responseString = sb.toString();
            EntityUtils.consume(response.getEntity());
            if (response.getStatusLine().getStatusCode() != 200) {
                //throw new RequestFailedException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode() + " " + responseString);
                throw new RequestFailedException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode() + " " + responseString, objectMapper.readValue(responseString, RequestFailedException.class));
            }


            return converResponseData(responseString, responseClass);

        } catch (MalformedURLException mURLE) {
            throw new RequestFailedException("Malformed url", mURLE);
        } catch (JsonGenerationException jge) {
            throw new RequestFailedException("Json Generation Exception", jge);
        } catch (JsonMappingException jme) {
            throw new RequestFailedException("Json Mapping Exception", jme);
        } catch (IOException ioe) {
            throw new RequestFailedException("IOException", ioe);
        }
    }


    public String performDeleteRequest(String serviceMethod) throws RequestFailedException {
        try {
            HttpDelete httpDelete = new HttpDelete(this.url + serviceMethod);
            String userpass = this.userName + ":" + this.apiKey;
            httpDelete.addHeader("Authorization", "Basic " + new String(new Base64().encode(userpass.getBytes())));


            HttpResponse response = httpClient.execute(httpDelete);
            RequestConfig requestConfig = RequestConfig.custom()
                    .setConnectTimeout(timeoutValue)
                    .setSocketTimeout(timeoutValue)
                    .setConnectionRequestTimeout(timeoutValue)
                    .build();
            httpDelete.setConfig(requestConfig);
            if (response.getStatusLine().getStatusCode() ==204){
                ((CloseableHttpResponse) response).close();
                return "";
            }
            BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));

            StringBuffer sb = new StringBuffer();
            String output;
            // System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                sb.append(output);
                // System.out.println(output);
            }
            ((CloseableHttpResponse) response).close();
            ;
            String responseString = sb.toString();
            EntityUtils.consume(response.getEntity());
            if (response.getStatusLine().getStatusCode() != 200) {
                //throw new RequestFailedException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode() + " " + responseString);
                throw new RequestFailedException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode() + " " + responseString, objectMapper.readValue(responseString, RequestFailedException.class));
            }
            return responseString;


        } catch (MalformedURLException mURLE) {
            throw new RequestFailedException("Malformed url", mURLE);
        } catch (JsonGenerationException jge) {
            throw new RequestFailedException("Json Generation Exception", jge);
        } catch (JsonMappingException jme) {
            throw new RequestFailedException("Json Mapping Exception", jme);
        } catch (IOException ioe) {
            throw new RequestFailedException("IOException", ioe);
        }
    }


    public BaseMailchimpResponse performPostRequest(BaseMalichimpRequest baseMalichimpRequest, String serviceMethod, Object responseClass) throws RequestFailedException {
        try {
            HttpPost httpPost = new HttpPost(this.url + serviceMethod);
            String userpass = this.userName + ":" + this.apiKey;
            httpPost.addHeader("Authorization", "Basic " + new String(new Base64().encode(userpass.getBytes())));

            String postData = getPostData(baseMalichimpRequest);
            StringEntity input = new StringEntity(postData, "UTF-8");
            input.setContentType("application/json");

            httpPost.setEntity(input);

            HttpResponse response = httpClient.execute(httpPost);
            RequestConfig requestConfig = RequestConfig.custom()
                    .setConnectTimeout(timeoutValue)
                    .setSocketTimeout(timeoutValue)
                    .setConnectionRequestTimeout(timeoutValue)
                    .build();
            httpPost.setConfig(requestConfig);

            BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));

            StringBuffer sb = new StringBuffer();
            String output;
            // System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                sb.append(output);
                // System.out.println(output);
            }

            String responseString = sb.toString();
            EntityUtils.consume(response.getEntity());
            if (response.getStatusLine().getStatusCode() != 200) {
                //throw new RequestFailedException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode() + " " + responseString);
                throw new RequestFailedException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode() + " " + responseString, objectMapper.readValue(responseString, RequestFailedException.class));
            }
            ((CloseableHttpResponse) response).close();

            return converResponseData(responseString, responseClass);

        } catch (MalformedURLException mURLE) {
            throw new RequestFailedException("Malformed url", mURLE);
        } catch (JsonGenerationException jge) {
            throw new RequestFailedException("Json Generation Exception", jge);
        } catch (JsonMappingException jme) {
            throw new RequestFailedException("Json Mapping Exception", jme);
        } catch (IOException ioe) {
            throw new RequestFailedException("IOException", ioe);
        }
    }


    private String getPostData(BaseMalichimpRequest baseMailchimpRequest) throws JsonProcessingException {
        return objectMapper.writeValueAsString(baseMailchimpRequest);
    }

    private BaseMailchimpResponse converResponseData(String json, Object responseClass) throws IOException {
        return objectMapper.readValue(json, (Class<BaseMailchimpResponse>) responseClass);
    }

}
