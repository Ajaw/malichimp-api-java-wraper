package Client;

public class MailChimpClientBuilder {

    private String apiKey;
    private String url;
    private Integer timeoutValue;
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void withUserName(String userName) {
        this.userName = userName;
    }

    public Integer getTimeoutValue() {
        return timeoutValue;
    }

    public void withTimeoutValue(Integer timeoutValue) {
        this.timeoutValue = timeoutValue;
    }

    public String getApiKey() {
        return apiKey;
    }

    public String getUrl() {
        return url;
    }

    public void withApiKey(String _apiKey) {
        this.apiKey = _apiKey;
    }

    public void withUrl(String _url) {
        this.url = _url;
    }

    public MailchimpClient build() {
        return new MailchimpClient(this);
    }

}
