package Methods.Campaings.Reponse;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Condition {
    private String field;
    private String op;
    private long value;

    @JsonProperty("field")
    public String getField() {
        return field;
    }

    @JsonProperty("field")
    public void setField(String value) {
        this.field = value;
    }

    @JsonProperty("op")
    public String getOp() {
        return op;
    }

    @JsonProperty("op")
    public void setOp(String value) {
        this.op = value;
    }

    @JsonProperty("value")
    public long getValue() {
        return value;
    }

    @JsonProperty("value")
    public void setValue(long value) {
        this.value = value;
    }
}
