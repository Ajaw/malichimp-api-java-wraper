package Methods.Campaings.Reponse;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ReportSummary {
    private long opens;
    private long uniqueOpens;
    private long openRate;
    private long clicks;
    private long subscriberClicks;
    private long clickRate;

    @JsonProperty("opens")
    public long getOpens() {
        return opens;
    }

    @JsonProperty("opens")
    public void setOpens(long value) {
        this.opens = value;
    }

    @JsonProperty("unique_opens")
    public long getUniqueOpens() {
        return uniqueOpens;
    }

    @JsonProperty("unique_opens")
    public void setUniqueOpens(long value) {
        this.uniqueOpens = value;
    }

    @JsonProperty("open_rate")
    public long getOpenRate() {
        return openRate;
    }

    @JsonProperty("open_rate")
    public void setOpenRate(long value) {
        this.openRate = value;
    }

    @JsonProperty("clicks")
    public long getClicks() {
        return clicks;
    }

    @JsonProperty("clicks")
    public void setClicks(long value) {
        this.clicks = value;
    }

    @JsonProperty("subscriber_clicks")
    public long getSubscriberClicks() {
        return subscriberClicks;
    }

    @JsonProperty("subscriber_clicks")
    public void setSubscriberClicks(long value) {
        this.subscriberClicks = value;
    }

    @JsonProperty("click_rate")
    public long getClickRate() {
        return clickRate;
    }

    @JsonProperty("click_rate")
    public void setClickRate(long value) {
        this.clickRate = value;
    }
}
