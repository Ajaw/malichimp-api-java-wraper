package Methods.Campaings.Reponse;

import Methods.BaseMailchimpResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Campaign extends BaseMailchimpResponse {
    private String id;
    private String type;
    private String createTime;
    private String archiveURL;
    private String status;
    private long emailsSent;
    private String sendTime;
    private String contentType;
    private Recipients recipients;
    private Settings settings;
    private Tracking tracking;
    private DeliveryStatus deliveryStatus;
    private Link[] links;
    private ReportSummary reportSummary;

    @JsonProperty("id")
    public String getID() {
        return id;
    }

    @JsonProperty("id")
    public void setID(String value) {
        this.id = value;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String value) {
        this.type = value;
    }

    @JsonProperty("create_time")
    public String getCreateTime() {
        return createTime;
    }

    @JsonProperty("create_time")
    public void setCreateTime(String value) {
        this.createTime = value;
    }

    @JsonProperty("archive_url")
    public String getArchiveURL() {
        return archiveURL;
    }

    @JsonProperty("archive_url")
    public void setArchiveURL(String value) {
        this.archiveURL = value;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String value) {
        this.status = value;
    }

    @JsonProperty("emails_sent")
    public long getEmailsSent() {
        return emailsSent;
    }

    @JsonProperty("emails_sent")
    public void setEmailsSent(long value) {
        this.emailsSent = value;
    }

    @JsonProperty("send_time")
    public String getSendTime() {
        return sendTime;
    }

    @JsonProperty("send_time")
    public void setSendTime(String value) {
        this.sendTime = value;
    }

    @JsonProperty("content_type")
    public String getContentType() {
        return contentType;
    }

    @JsonProperty("content_type")
    public void setContentType(String value) {
        this.contentType = value;
    }

    @JsonProperty("recipients")
    public Recipients getRecipients() {
        return recipients;
    }

    @JsonProperty("recipients")
    public void setRecipients(Recipients value) {
        this.recipients = value;
    }

    @JsonProperty("settings")
    public Settings getSettings() {
        return settings;
    }

    @JsonProperty("settings")
    public void setSettings(Settings value) {
        this.settings = value;
    }

    @JsonProperty("tracking")
    public Tracking getTracking() {
        return tracking;
    }

    @JsonProperty("tracking")
    public void setTracking(Tracking value) {
        this.tracking = value;
    }

    @JsonProperty("delivery_status")
    public DeliveryStatus getDeliveryStatus() {
        return deliveryStatus;
    }

    @JsonProperty("delivery_status")
    public void setDeliveryStatus(DeliveryStatus value) {
        this.deliveryStatus = value;
    }

    @JsonProperty("_links")
    public Link[] getLinks() {
        return links;
    }

    @JsonProperty("_links")
    public void setLinks(Link[] value) {
        this.links = value;
    }

    @JsonProperty("report_summary")
    public ReportSummary getReportSummary() {
        return reportSummary;
    }

    @JsonProperty("report_summary")
    public void setReportSummary(ReportSummary value) {
        this.reportSummary = value;
    }
}
