package Methods.Campaings.Reponse;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Recipients {
    private String listID;
    private String segmentText;
    private SegmentOpts segmentOpts;

    @JsonProperty("list_id")
    public String getListID() {
        return listID;
    }

    @JsonProperty("list_id")
    public void setListID(String value) {
        this.listID = value;
    }

    @JsonProperty("segment_text")
    public String getSegmentText() {
        return segmentText;
    }

    @JsonProperty("segment_text")
    public void setSegmentText(String value) {
        this.segmentText = value;
    }

    @JsonProperty("segment_opts")
    public SegmentOpts getSegmentOpts() {
        return segmentOpts;
    }

    @JsonProperty("segment_opts")
    public void setSegmentOpts(SegmentOpts value) {
        this.segmentOpts = value;
    }
}
