package Methods.Campaings.Reponse;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SegmentOpts {
    private long savedSegmentID;
    private String match;
    private Condition[] conditions;

    @JsonProperty("saved_segment_id")
    public long getSavedSegmentID() {
        return savedSegmentID;
    }

    @JsonProperty("saved_segment_id")
    public void setSavedSegmentID(long value) {
        this.savedSegmentID = value;
    }

    @JsonProperty("match")
    public String getMatch() {
        return match;
    }

    @JsonProperty("match")
    public void setMatch(String value) {
        this.match = value;
    }

    @JsonProperty("conditions")
    public Condition[] getConditions() {
        return conditions;
    }

    @JsonProperty("conditions")
    public void setConditions(Condition[] value) {
        this.conditions = value;
    }
}
