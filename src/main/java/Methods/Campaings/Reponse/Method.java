package Methods.Campaings.Reponse;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.io.IOException;

public enum Method {
    DELETE, GET, POST;

    @JsonCreator
    public static Method forValue(String value) throws IOException {
        if (value.equals("DELETE")) return DELETE;
        if (value.equals("GET")) return GET;
        if (value.equals("POST")) return POST;
        throw new IOException("Cannot deserialize Method");
    }

    @JsonValue
    public String toValue() {
        switch (this) {
            case DELETE:
                return "DELETE";
            case GET:
                return "GET";
            case POST:
                return "POST";
        }
        return null;
    }
}
