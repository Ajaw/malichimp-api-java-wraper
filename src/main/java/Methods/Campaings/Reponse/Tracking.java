package Methods.Campaings.Reponse;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Tracking {
    private boolean opens;
    private boolean htmlClicks;
    private boolean textClicks;
    private boolean goalTracking;
    private boolean ecomm360;
    private boolean googleAnalytics;
    private String clicktale;

    @JsonProperty("opens")
    public boolean getOpens() {
        return opens;
    }

    @JsonProperty("opens")
    public void setOpens(boolean value) {
        this.opens = value;
    }

    @JsonProperty("html_clicks")
    public boolean getHTMLClicks() {
        return htmlClicks;
    }

    @JsonProperty("html_clicks")
    public void setHTMLClicks(boolean value) {
        this.htmlClicks = value;
    }

    @JsonProperty("text_clicks")
    public boolean getTextClicks() {
        return textClicks;
    }

    @JsonProperty("text_clicks")
    public void setTextClicks(boolean value) {
        this.textClicks = value;
    }

    @JsonProperty("goal_tracking")
    public boolean getGoalTracking() {
        return goalTracking;
    }

    @JsonProperty("goal_tracking")
    public void setGoalTracking(boolean value) {
        this.goalTracking = value;
    }

    @JsonProperty("ecomm360")
    public boolean getEcomm360() {
        return ecomm360;
    }

    @JsonProperty("ecomm360")
    public void setEcomm360(boolean value) {
        this.ecomm360 = value;
    }

    @JsonProperty("google_analytics")
    public boolean getGoogleAnalytics() {
        return googleAnalytics;
    }

    @JsonProperty("google_analytics")
    public void setGoogleAnalytics(boolean value) {
        this.googleAnalytics = value;
    }

    @JsonProperty("clicktale")
    public String getClicktale() {
        return clicktale;
    }

    @JsonProperty("clicktale")
    public void setClicktale(String value) {
        this.clicktale = value;
    }
}
