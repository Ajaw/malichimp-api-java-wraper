package Methods.Campaings.Reponse;

import Methods.BaseMailchimpResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CampaignsResponse extends BaseMailchimpResponse {
    private Campaign[] campaigns;
    private long totalItems;
    private Link[] links;

    @JsonProperty("campaigns")
    public Campaign[] getCampaigns() {
        return campaigns;
    }

    @JsonProperty("campaigns")
    public void setCampaigns(Campaign[] value) {
        this.campaigns = value;
    }

    @JsonProperty("total_items")
    public long getTotalItems() {
        return totalItems;
    }

    @JsonProperty("total_items")
    public void setTotalItems(long value) {
        this.totalItems = value;
    }

    @JsonProperty("_links")
    public Link[] getLinks() {
        return links;
    }

    @JsonProperty("_links")
    public void setLinks(Link[] value) {
        this.links = value;
    }
}
