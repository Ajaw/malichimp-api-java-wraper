package Methods.Campaings.Reponse;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DeliveryStatus {
    private boolean enabled;

    @JsonProperty("enabled")
    public boolean getEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public void setEnabled(boolean value) {
        this.enabled = value;
    }
}
