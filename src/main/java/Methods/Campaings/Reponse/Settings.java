package Methods.Campaings.Reponse;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Settings {
    private String subjectLine;
    private String title;
    private String fromName;
    private String replyTo;
    private boolean useConversation;
    private String toName;
    private long folderID;
    private boolean authenticate;
    private boolean autoFooter;
    private boolean inlineCSS;
    private boolean autoTweet;
    private boolean fbComments;
    private boolean timewarp;
    private long templateID;
    private boolean dragAndDrop;

    @JsonProperty("subject_line")
    public String getSubjectLine() {
        return subjectLine;
    }

    @JsonProperty("subject_line")
    public void setSubjectLine(String value) {
        this.subjectLine = value;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String value) {
        this.title = value;
    }

    @JsonProperty("from_name")
    public String getFromName() {
        return fromName;
    }

    @JsonProperty("from_name")
    public void setFromName(String value) {
        this.fromName = value;
    }

    @JsonProperty("reply_to")
    public String getReplyTo() {
        return replyTo;
    }

    @JsonProperty("reply_to")
    public void setReplyTo(String value) {
        this.replyTo = value;
    }

    @JsonProperty("use_conversation")
    public boolean getUseConversation() {
        return useConversation;
    }

    @JsonProperty("use_conversation")
    public void setUseConversation(boolean value) {
        this.useConversation = value;
    }

    @JsonProperty("to_name")
    public String getToName() {
        return toName;
    }

    @JsonProperty("to_name")
    public void setToName(String value) {
        this.toName = value;
    }

    @JsonProperty("folder_id")
    public long getFolderID() {
        return folderID;
    }

    @JsonProperty("folder_id")
    public void setFolderID(long value) {
        this.folderID = value;
    }

    @JsonProperty("authenticate")
    public boolean getAuthenticate() {
        return authenticate;
    }

    @JsonProperty("authenticate")
    public void setAuthenticate(boolean value) {
        this.authenticate = value;
    }

    @JsonProperty("auto_footer")
    public boolean getAutoFooter() {
        return autoFooter;
    }

    @JsonProperty("auto_footer")
    public void setAutoFooter(boolean value) {
        this.autoFooter = value;
    }

    @JsonProperty("inline_css")
    public boolean getInlineCSS() {
        return inlineCSS;
    }

    @JsonProperty("inline_css")
    public void setInlineCSS(boolean value) {
        this.inlineCSS = value;
    }

    @JsonProperty("auto_tweet")
    public boolean getAutoTweet() {
        return autoTweet;
    }

    @JsonProperty("auto_tweet")
    public void setAutoTweet(boolean value) {
        this.autoTweet = value;
    }

    @JsonProperty("fb_comments")
    public boolean getFbComments() {
        return fbComments;
    }

    @JsonProperty("fb_comments")
    public void setFbComments(boolean value) {
        this.fbComments = value;
    }

    @JsonProperty("timewarp")
    public boolean getTimewarp() {
        return timewarp;
    }

    @JsonProperty("timewarp")
    public void setTimewarp(boolean value) {
        this.timewarp = value;
    }

    @JsonProperty("template_id")
    public long getTemplateID() {
        return templateID;
    }

    @JsonProperty("template_id")
    public void setTemplateID(long value) {
        this.templateID = value;
    }

    @JsonProperty("drag_and_drop")
    public boolean getDragAndDrop() {
        return dragAndDrop;
    }

    @JsonProperty("drag_and_drop")
    public void setDragAndDrop(boolean value) {
        this.dragAndDrop = value;
    }
}
