package Methods.Campaings.Request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Settings {
    private String subjectLine;
    private String replyTo;
    private String fromName;

    @JsonProperty("subject_line")
    public String getSubjectLine() {
        return subjectLine;
    }

    @JsonProperty("subject_line")
    public void setSubjectLine(String value) {
        this.subjectLine = value;
    }

    @JsonProperty("reply_to")
    public String getReplyTo() {
        return replyTo;
    }

    @JsonProperty("reply_to")
    public void setReplyTo(String value) {
        this.replyTo = value;
    }

    @JsonProperty("from_name")
    public String getFromName() {
        return fromName;
    }

    @JsonProperty("from_name")
    public void setFromName(String value) {
        this.fromName = value;
    }
}
