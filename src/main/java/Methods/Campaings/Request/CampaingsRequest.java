package Methods.Campaings.Request;

import Methods.BaseMalichimpRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CampaingsRequest extends BaseMalichimpRequest {
    private Recipients recipients;
    private String type;
    private Settings settings;

    @JsonProperty("recipients")
    public Recipients getRecipients() {
        return recipients;
    }

    @JsonProperty("recipients")
    public void setRecipients(Recipients value) {
        this.recipients = value;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String value) {
        this.type = value;
    }

    @JsonProperty("settings")
    public Settings getSettings() {
        return settings;
    }

    @JsonProperty("settings")
    public void setSettings(Settings value) {
        this.settings = value;
    }
}
