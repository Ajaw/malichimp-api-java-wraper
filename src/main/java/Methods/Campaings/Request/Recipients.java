package Methods.Campaings.Request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Recipients {
    private String listID;

    @JsonProperty("list_id")
    public String getListID() {
        return listID;
    }

    @JsonProperty("list_id")
    public void setListID(String value) {
        this.listID = value;
    }
}
