package Methods.Campaings;

import Client.MailchimpClient;
import Methods.Campaings.Reponse.Campaign;
import Methods.Campaings.Reponse.CampaignsResponse;
import Methods.Campaings.Request.CampaingsRequest;
import Methods.ServiceMethods;
import exception.RequestFailedException;

public class MailchimpCampaingsRequests {

    private final String textToReplace = "{campaign_id}";
    private MailchimpClient mailchimpClient;

    public MailchimpCampaingsRequests(MailchimpClient mailchimpClient) {
        this.mailchimpClient = mailchimpClient;
    }

    public Campaign createCampaing(CampaingsRequest campaingsRequest) throws RequestFailedException {

        return (Campaign) mailchimpClient.performPostRequest(campaingsRequest, ServiceMethods.campaings, Campaign.class);
    }

    public CampaignsResponse getCampaings() throws RequestFailedException {
        return (CampaignsResponse) mailchimpClient.performGetRequest(ServiceMethods.campaings, CampaignsResponse.class);
    }

    public Campaign getSingleCampaign(String campaignId) throws RequestFailedException {
        String req = ServiceMethods.getSingleList;
        req = req.replace(this.textToReplace, campaignId);
        return (Campaign) mailchimpClient.performGetRequest(req, Campaign.class);
    }

    public String deleteCampaign(String campaignId) throws RequestFailedException {
        String req = ServiceMethods.deleteCampaing;
        req = req.replace(this.textToReplace, campaignId);
        return (String) mailchimpClient.performDeleteRequest(req);
    }


}
