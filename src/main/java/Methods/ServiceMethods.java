package Methods;

public class ServiceMethods {

    public static final String lists = "/lists";
    public static final String deleteLists = "/lists/{list_id}";
    public static final String getSingleList = "/lists/{list_id}";

    public static final String campaings = "/campaigns";
    public static final String getSinglecampaing = "/campaigns/{campaign_id}";
    public static final String deleteCampaing = "/campaigns/{campaign_id}";


    public static final String addNewMember = "/lists/{list_id}/members";
    public static final String getAllMembers = "/lists/{list_id}/members";
    public static final String getSingleMember = "/lists/{list_id}/members/{subscriber_hash}";
    public static final String deleteMember = "/lists/{list_id}/members/{subscriber_hash}";

    public static final String getEmailActivityReportForCampaing = "/reports/{campaign_id}/email-activity";
    public static final String getEmailActivityReportForSingleSubscriber = "/reports/{campaign_id}/email-activity/{subscriber_hash}";

    public static final String getUnsubscribeReportForCampaing = "/reports/{campaign_id}/unsubscribed";
    public static final String getUnsubscribeReportForSingleSubscriber = "/reports/{campaign_id}/unsubscribed/{subscriber_hash}";

}
