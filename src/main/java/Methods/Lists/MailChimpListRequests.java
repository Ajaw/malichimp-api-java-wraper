package Methods.Lists;

import Client.MailchimpClient;
import Methods.Lists.Request.ListsRequest;
import Methods.Lists.Response.List;
import Methods.Lists.Response.ListsResponse;
import Methods.ServiceMethods;
import exception.RequestFailedException;

public class MailChimpListRequests {


    private MailchimpClient mailchimpClient;

    public MailChimpListRequests(MailchimpClient mailchimpClient) {
        this.mailchimpClient = mailchimpClient;
    }

    public List createList(ListsRequest listsRequest) throws RequestFailedException {

        return (List) mailchimpClient.performPostRequest(listsRequest, ServiceMethods.lists, List.class);
    }

    public ListsResponse getLists() throws RequestFailedException {
        return (ListsResponse) mailchimpClient.performGetRequest(ServiceMethods.lists, ListsResponse.class);
    }

    public ListsResponse getSingleList(String listId) throws RequestFailedException {
        String req = ServiceMethods.getSingleList;
        req = req.replace("{list_id}", listId);
        return (ListsResponse) mailchimpClient.performGetRequest(req, ListsResponse.class);
    }

    public String deleteList(String listId) throws RequestFailedException {
        String req = ServiceMethods.deleteLists;
        req = req.replace("{list_id}", listId);
        return (String) mailchimpClient.performDeleteRequest(req);
    }

}
