package Methods.Lists.Members.Request;

import Methods.BaseMalichimpRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MembersRequest extends BaseMalichimpRequest {
    private String emailAddress;
    private String status;
    private String[] tags;

    @JsonProperty("email_address")
    public String getEmailAddress() {
        return emailAddress;
    }

    @JsonProperty("email_address")
    public void setEmailAddress(String value) {
        this.emailAddress = value;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String value) {
        this.status = value;
    }

    @JsonProperty("tags")
    public String[] getTags() {
        return tags;
    }

    @JsonProperty("tags")
    public void setTags(String[] value) {
        this.tags = value;
    }
}
