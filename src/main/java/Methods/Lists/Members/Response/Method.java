package Methods.Lists.Members.Response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.io.IOException;

public enum Method {
    DELETE, GET, PATCH, POST, PUT;

    @JsonCreator
    public static Method forValue(String value) throws IOException {
        if (value.equals("DELETE")) return DELETE;
        if (value.equals("GET")) return GET;
        if (value.equals("PATCH")) return PATCH;
        if (value.equals("POST")) return POST;
        if (value.equals("PUT")) return PUT;
        throw new IOException("Cannot deserialize Method");
    }

    @JsonValue
    public String toValue() {
        switch (this) {
            case DELETE:
                return "DELETE";
            case GET:
                return "GET";
            case PATCH:
                return "PATCH";
            case POST:
                return "POST";
            case PUT:
                return "PUT";
        }
        return null;
    }
}
