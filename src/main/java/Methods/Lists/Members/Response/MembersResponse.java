package Methods.Lists.Members.Response;

import Methods.BaseMailchimpResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

public class MembersResponse extends BaseMailchimpResponse {
    private Member[] members;
    private String listID;
    private Link[] links;
    private long totalItems;

    @JsonProperty("members")
    public Member[] getMembers() {
        return members;
    }

    @JsonProperty("members")
    public void setMembers(Member[] value) {
        this.members = value;
    }

    @JsonProperty("list_id")
    public String getListID() {
        return listID;
    }

    @JsonProperty("list_id")
    public void setListID(String value) {
        this.listID = value;
    }

    @JsonProperty("_links")
    public Link[] getLinks() {
        return links;
    }

    @JsonProperty("_links")
    public void setLinks(Link[] value) {
        this.links = value;
    }

    @JsonProperty("total_items")
    public long getTotalItems() {
        return totalItems;
    }

    @JsonProperty("total_items")
    public void setTotalItems(long value) {
        this.totalItems = value;
    }
}
