package Methods.Lists.Members.Response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LastNote {
    private long noteID;
    private String createdAt;
    private String createdBy;
    private String note;

    @JsonProperty("note_id")
    public long getNoteID() {
        return noteID;
    }

    @JsonProperty("note_id")
    public void setNoteID(long value) {
        this.noteID = value;
    }

    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("created_at")
    public void setCreatedAt(String value) {
        this.createdAt = value;
    }

    @JsonProperty("created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    @JsonProperty("created_by")
    public void setCreatedBy(String value) {
        this.createdBy = value;
    }

    @JsonProperty("note")
    public String getNote() {
        return note;
    }

    @JsonProperty("note")
    public void setNote(String value) {
        this.note = value;
    }
}
