package Methods.Lists.Members.Response;

import Methods.BaseMailchimpResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

public class Member extends BaseMailchimpResponse {
    private String id;
    private String emailAddress;
    private String uniqueEmailID;
    private String emailType;
    private String status;
    private String statusIfNew;
    private MergeFields mergeFields;
    private Map<String, Boolean> interests;
    private Stats stats;
    private String ipSignup;
    private String timestampSignup;
    private String ipOpt;
    private String timestampOpt;
    private long memberRating;
    private String lastChanged;
    private String language;
    private boolean vip;
    private String emailClient;
    private Location location;
    private String listID;
    private Link[] links;
    private LastNote lastNote;

    @JsonProperty("id")
    public String getID() {
        return id;
    }

    @JsonProperty("id")
    public void setID(String value) {
        this.id = value;
    }

    @JsonProperty("email_address")
    public String getEmailAddress() {
        return emailAddress;
    }

    @JsonProperty("email_address")
    public void setEmailAddress(String value) {
        this.emailAddress = value;
    }

    @JsonProperty("unique_email_id")
    public String getUniqueEmailID() {
        return uniqueEmailID;
    }

    @JsonProperty("unique_email_id")
    public void setUniqueEmailID(String value) {
        this.uniqueEmailID = value;
    }

    @JsonProperty("email_type")
    public String getEmailType() {
        return emailType;
    }

    @JsonProperty("email_type")
    public void setEmailType(String value) {
        this.emailType = value;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String value) {
        this.status = value;
    }

    @JsonProperty("status_if_new")
    public String getStatusIfNew() {
        return statusIfNew;
    }

    @JsonProperty("status_if_new")
    public void setStatusIfNew(String value) {
        this.statusIfNew = value;
    }

    @JsonProperty("merge_fields")
    public MergeFields getMergeFields() {
        return mergeFields;
    }

    @JsonProperty("merge_fields")
    public void setMergeFields(MergeFields value) {
        this.mergeFields = value;
    }

    @JsonProperty("interests")
    public Map<String, Boolean> getInterests() {
        return interests;
    }

    @JsonProperty("interests")
    public void setInterests(Map<String, Boolean> value) {
        this.interests = value;
    }

    @JsonProperty("stats")
    public Stats getStats() {
        return stats;
    }

    @JsonProperty("stats")
    public void setStats(Stats value) {
        this.stats = value;
    }

    @JsonProperty("ip_signup")
    public String getIPSignup() {
        return ipSignup;
    }

    @JsonProperty("ip_signup")
    public void setIPSignup(String value) {
        this.ipSignup = value;
    }

    @JsonProperty("timestamp_signup")
    public String getTimestampSignup() {
        return timestampSignup;
    }

    @JsonProperty("timestamp_signup")
    public void setTimestampSignup(String value) {
        this.timestampSignup = value;
    }

    @JsonProperty("ip_opt")
    public String getIPOpt() {
        return ipOpt;
    }

    @JsonProperty("ip_opt")
    public void setIPOpt(String value) {
        this.ipOpt = value;
    }

    @JsonProperty("timestamp_opt")
    public String getTimestampOpt() {
        return timestampOpt;
    }

    @JsonProperty("timestamp_opt")
    public void setTimestampOpt(String value) {
        this.timestampOpt = value;
    }

    @JsonProperty("member_rating")
    public long getMemberRating() {
        return memberRating;
    }

    @JsonProperty("member_rating")
    public void setMemberRating(long value) {
        this.memberRating = value;
    }

    @JsonProperty("last_changed")
    public String getLastChanged() {
        return lastChanged;
    }

    @JsonProperty("last_changed")
    public void setLastChanged(String value) {
        this.lastChanged = value;
    }

    @JsonProperty("language")
    public String getLanguage() {
        return language;
    }

    @JsonProperty("language")
    public void setLanguage(String value) {
        this.language = value;
    }

    @JsonProperty("vip")
    public boolean getVip() {
        return vip;
    }

    @JsonProperty("vip")
    public void setVip(boolean value) {
        this.vip = value;
    }

    @JsonProperty("email_client")
    public String getEmailClient() {
        return emailClient;
    }

    @JsonProperty("email_client")
    public void setEmailClient(String value) {
        this.emailClient = value;
    }

    @JsonProperty("location")
    public Location getLocation() {
        return location;
    }

    @JsonProperty("location")
    public void setLocation(Location value) {
        this.location = value;
    }

    @JsonProperty("list_id")
    public String getListID() {
        return listID;
    }

    @JsonProperty("list_id")
    public void setListID(String value) {
        this.listID = value;
    }

    @JsonProperty("_links")
    public Link[] getLinks() {
        return links;
    }

    @JsonProperty("_links")
    public void setLinks(Link[] value) {
        this.links = value;
    }

    @JsonProperty("last_note")
    public LastNote getLastNote() {
        return lastNote;
    }

    @JsonProperty("last_note")
    public void setLastNote(LastNote value) {
        this.lastNote = value;
    }
}
