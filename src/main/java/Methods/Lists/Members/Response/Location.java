package Methods.Lists.Members.Response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Location {
    private double latitude;
    private double longitude;
    private long gmtoff;
    private long dstoff;
    private String countryCode;
    private String timezone;

    @JsonProperty("latitude")
    public double getLatitude() {
        return latitude;
    }

    @JsonProperty("latitude")
    public void setLatitude(double value) {
        this.latitude = value;
    }

    @JsonProperty("longitude")
    public double getLongitude() {
        return longitude;
    }

    @JsonProperty("longitude")
    public void setLongitude(double value) {
        this.longitude = value;
    }

    @JsonProperty("gmtoff")
    public long getGmtoff() {
        return gmtoff;
    }

    @JsonProperty("gmtoff")
    public void setGmtoff(long value) {
        this.gmtoff = value;
    }

    @JsonProperty("dstoff")
    public long getDstoff() {
        return dstoff;
    }

    @JsonProperty("dstoff")
    public void setDstoff(long value) {
        this.dstoff = value;
    }

    @JsonProperty("country_code")
    public String getCountryCode() {
        return countryCode;
    }

    @JsonProperty("country_code")
    public void setCountryCode(String value) {
        this.countryCode = value;
    }

    @JsonProperty("timezone")
    public String getTimezone() {
        return timezone;
    }

    @JsonProperty("timezone")
    public void setTimezone(String value) {
        this.timezone = value;
    }
}
