package Methods.Lists.Members.Response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Stats {
    private long avgOpenRate;
    private long avgClickRate;

    @JsonProperty("avg_open_rate")
    public long getAvgOpenRate() {
        return avgOpenRate;
    }

    @JsonProperty("avg_open_rate")
    public void setAvgOpenRate(long value) {
        this.avgOpenRate = value;
    }

    @JsonProperty("avg_click_rate")
    public long getAvgClickRate() {
        return avgClickRate;
    }

    @JsonProperty("avg_click_rate")
    public void setAvgClickRate(long value) {
        this.avgClickRate = value;
    }
}
