package Methods.Lists.Members.Response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MergeFields {
    private String fname;
    private String lname;

    @JsonProperty("FNAME")
    public String getFname() {
        return fname;
    }

    @JsonProperty("FNAME")
    public void setFname(String value) {
        this.fname = value;
    }

    @JsonProperty("LNAME")
    public String getLname() {
        return lname;
    }

    @JsonProperty("LNAME")
    public void setLname(String value) {
        this.lname = value;
    }
}
