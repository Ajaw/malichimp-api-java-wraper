package Methods.Lists.Members.Response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Link {
    private String rel;
    private String href;
    private Method method;
    private String targetSchema;
    private String schema;

    @JsonProperty("rel")
    public String getRel() {
        return rel;
    }

    @JsonProperty("rel")
    public void setRel(String value) {
        this.rel = value;
    }

    @JsonProperty("href")
    public String getHref() {
        return href;
    }

    @JsonProperty("href")
    public void setHref(String value) {
        this.href = value;
    }

    @JsonProperty("method")
    public Method getMethod() {
        return method;
    }

    @JsonProperty("method")
    public void setMethod(Method value) {
        this.method = value;
    }

    @JsonProperty("targetSchema")
    public String getTargetSchema() {
        return targetSchema;
    }

    @JsonProperty("targetSchema")
    public void setTargetSchema(String value) {
        this.targetSchema = value;
    }

    @JsonProperty("schema")
    public String getSchema() {
        return schema;
    }

    @JsonProperty("schema")
    public void setSchema(String value) {
        this.schema = value;
    }
}
