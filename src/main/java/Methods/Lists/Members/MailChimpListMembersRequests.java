package Methods.Lists.Members;

import Client.MailchimpClient;
import Methods.Lists.Members.Request.MembersRequest;
import Methods.Lists.Members.Response.Member;
import Methods.Lists.Members.Response.MembersResponse;
import Methods.ServiceMethods;
import exception.RequestFailedException;

public class MailChimpListMembersRequests {


    private final String textToReplace = "{list_id}";
    private final String subscriberHash = "{subscriber_hash}";
    private MailchimpClient mailchimpClient;

    public MailChimpListMembersRequests(MailchimpClient mailchimpClient) {
        this.mailchimpClient = mailchimpClient;
    }

    public Member createMember(String listId,MembersRequest campaingsRequest) throws RequestFailedException {
        String req = ServiceMethods.addNewMember;
        req = req.replace(this.textToReplace, listId);
        return (Member) mailchimpClient.performPostRequest(campaingsRequest, req, Member.class);
    }

    public MembersResponse getMembers(String listId) throws RequestFailedException {
        String req = ServiceMethods.getAllMembers;
        req = req.replace(this.textToReplace, listId);
        return (MembersResponse) mailchimpClient.performGetRequest(req, MembersResponse.class);
    }

    public Member getSingleMember(String listId, String subscriberHash) throws RequestFailedException {
        String req = ServiceMethods.getSingleList;
        req = req.replace(this.textToReplace, listId);
        req = req.replace(this.subscriberHash, subscriberHash);
        return (Member) mailchimpClient.performGetRequest(req, Member.class);
    }

    public String deleteMember(String listId, String subscriberHash) throws RequestFailedException {
        String req = ServiceMethods.deleteMember;
        req = req.replace(this.textToReplace, listId);
        req = req.replace(this.subscriberHash, subscriberHash);
        return (String) mailchimpClient.performDeleteRequest(req);
    }

}
