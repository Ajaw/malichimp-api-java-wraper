package Methods.Lists.Response;

import Methods.BaseMailchimpResponse;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ListsResponse extends BaseMailchimpResponse {
    private List[] lists;
    private Link[] links;
    private long totalItems;

    @JsonProperty("lists")
    public List[] getLists() {
        return lists;
    }

    @JsonProperty("lists")
    public void setLists(List[] value) {
        this.lists = value;
    }

    @JsonProperty("_links")
    public Link[] getLinks() {
        return links;
    }

    @JsonProperty("_links")
    public void setLinks(Link[] value) {
        this.links = value;
    }

    @JsonProperty("total_items")
    public long getTotalItems() {
        return totalItems;
    }

    @JsonProperty("total_items")
    public void setTotalItems(long value) {
        this.totalItems = value;
    }
}
