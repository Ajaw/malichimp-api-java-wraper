package Methods.Lists.Response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CampaignDefaults {
    private String fromName;
    private String fromEmail;
    private String subject;
    private String language;

    @JsonProperty("from_name")
    public String getFromName() {
        return fromName;
    }

    @JsonProperty("from_name")
    public void setFromName(String value) {
        this.fromName = value;
    }

    @JsonProperty("from_email")
    public String getFromEmail() {
        return fromEmail;
    }

    @JsonProperty("from_email")
    public void setFromEmail(String value) {
        this.fromEmail = value;
    }

    @JsonProperty("subject")
    public String getSubject() {
        return subject;
    }

    @JsonProperty("subject")
    public void setSubject(String value) {
        this.subject = value;
    }

    @JsonProperty("language")
    public String getLanguage() {
        return language;
    }

    @JsonProperty("language")
    public void setLanguage(String value) {
        this.language = value;
    }
}
