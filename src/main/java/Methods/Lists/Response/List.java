package Methods.Lists.Response;

import Methods.BaseMailchimpResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class List extends BaseMailchimpResponse {
    private String id;
    private String name;
    private Contact contact;
    private String permissionReminder;
    private boolean useArchiveBar;
    private CampaignDefaults campaignDefaults;
    private String notifyOnSubscribe;
    private String notifyOnUnsubscribe;
    private String dateCreated;
    private long listRating;
    private boolean emailTypeOption;
    private String subscribeURLShort;
    private String subscribeURLLong;
    private String beamerAddress;
    private String visibility;
    private Object[] modules;
    private Stats stats;
    private Link[] links;

    @JsonProperty("id")
    public String getID() {
        return id;
    }

    @JsonProperty("id")
    public void setID(String value) {
        this.id = value;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String value) {
        this.name = value;
    }

    @JsonProperty("contact")
    public Contact getContact() {
        return contact;
    }

    @JsonProperty("contact")
    public void setContact(Contact value) {
        this.contact = value;
    }

    @JsonProperty("permission_reminder")
    public String getPermissionReminder() {
        return permissionReminder;
    }

    @JsonProperty("permission_reminder")
    public void setPermissionReminder(String value) {
        this.permissionReminder = value;
    }

    @JsonProperty("use_archive_bar")
    public boolean getUseArchiveBar() {
        return useArchiveBar;
    }

    @JsonProperty("use_archive_bar")
    public void setUseArchiveBar(boolean value) {
        this.useArchiveBar = value;
    }

    @JsonProperty("campaign_defaults")
    public CampaignDefaults getCampaignDefaults() {
        return campaignDefaults;
    }

    @JsonProperty("campaign_defaults")
    public void setCampaignDefaults(CampaignDefaults value) {
        this.campaignDefaults = value;
    }

    @JsonProperty("notify_on_subscribe")
    public String getNotifyOnSubscribe() {
        return notifyOnSubscribe;
    }

    @JsonProperty("notify_on_subscribe")
    public void setNotifyOnSubscribe(String value) {
        this.notifyOnSubscribe = value;
    }

    @JsonProperty("notify_on_unsubscribe")
    public String getNotifyOnUnsubscribe() {
        return notifyOnUnsubscribe;
    }

    @JsonProperty("notify_on_unsubscribe")
    public void setNotifyOnUnsubscribe(String value) {
        this.notifyOnUnsubscribe = value;
    }

    @JsonProperty("date_created")
    public String getDateCreated() {
        return dateCreated;
    }

    @JsonProperty("date_created")
    public void setDateCreated(String value) {
        this.dateCreated = value;
    }

    @JsonProperty("list_rating")
    public long getListRating() {
        return listRating;
    }

    @JsonProperty("list_rating")
    public void setListRating(long value) {
        this.listRating = value;
    }

    @JsonProperty("email_type_option")
    public boolean getEmailTypeOption() {
        return emailTypeOption;
    }

    @JsonProperty("email_type_option")
    public void setEmailTypeOption(boolean value) {
        this.emailTypeOption = value;
    }

    @JsonProperty("subscribe_url_short")
    public String getSubscribeURLShort() {
        return subscribeURLShort;
    }

    @JsonProperty("subscribe_url_short")
    public void setSubscribeURLShort(String value) {
        this.subscribeURLShort = value;
    }

    @JsonProperty("subscribe_url_long")
    public String getSubscribeURLLong() {
        return subscribeURLLong;
    }

    @JsonProperty("subscribe_url_long")
    public void setSubscribeURLLong(String value) {
        this.subscribeURLLong = value;
    }

    @JsonProperty("beamer_address")
    public String getBeamerAddress() {
        return beamerAddress;
    }

    @JsonProperty("beamer_address")
    public void setBeamerAddress(String value) {
        this.beamerAddress = value;
    }

    @JsonProperty("visibility")
    public String getVisibility() {
        return visibility;
    }

    @JsonProperty("visibility")
    public void setVisibility(String value) {
        this.visibility = value;
    }

    @JsonProperty("modules")
    public Object[] getModules() {
        return modules;
    }

    @JsonProperty("modules")
    public void setModules(Object[] value) {
        this.modules = value;
    }

    @JsonProperty("stats")
    public Stats getStats() {
        return stats;
    }

    @JsonProperty("stats")
    public void setStats(Stats value) {
        this.stats = value;
    }

    @JsonProperty("_links")
    public Link[] getLinks() {
        return links;
    }

    @JsonProperty("_links")
    public void setLinks(Link[] value) {
        this.links = value;
    }
}
