package Methods.Lists.Response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Stats {
    private long memberCount;
    private long unsubscribeCount;
    private long cleanedCount;
    private long memberCountSinceSend;
    private long unsubscribeCountSinceSend;
    private long cleanedCountSinceSend;
    private long campaignCount;
    private String campaignLastSent;
    private long mergeFieldCount;
    private long avgSubRate;
    private long avgUnsubRate;
    private long targetSubRate;
    private long openRate;
    private long clickRate;
    private String lastSubDate;
    private String lastUnsubDate;

    @JsonProperty("member_count")
    public long getMemberCount() {
        return memberCount;
    }

    @JsonProperty("member_count")
    public void setMemberCount(long value) {
        this.memberCount = value;
    }

    @JsonProperty("unsubscribe_count")
    public long getUnsubscribeCount() {
        return unsubscribeCount;
    }

    @JsonProperty("unsubscribe_count")
    public void setUnsubscribeCount(long value) {
        this.unsubscribeCount = value;
    }

    @JsonProperty("cleaned_count")
    public long getCleanedCount() {
        return cleanedCount;
    }

    @JsonProperty("cleaned_count")
    public void setCleanedCount(long value) {
        this.cleanedCount = value;
    }

    @JsonProperty("member_count_since_send")
    public long getMemberCountSinceSend() {
        return memberCountSinceSend;
    }

    @JsonProperty("member_count_since_send")
    public void setMemberCountSinceSend(long value) {
        this.memberCountSinceSend = value;
    }

    @JsonProperty("unsubscribe_count_since_send")
    public long getUnsubscribeCountSinceSend() {
        return unsubscribeCountSinceSend;
    }

    @JsonProperty("unsubscribe_count_since_send")
    public void setUnsubscribeCountSinceSend(long value) {
        this.unsubscribeCountSinceSend = value;
    }

    @JsonProperty("cleaned_count_since_send")
    public long getCleanedCountSinceSend() {
        return cleanedCountSinceSend;
    }

    @JsonProperty("cleaned_count_since_send")
    public void setCleanedCountSinceSend(long value) {
        this.cleanedCountSinceSend = value;
    }

    @JsonProperty("campaign_count")
    public long getCampaignCount() {
        return campaignCount;
    }

    @JsonProperty("campaign_count")
    public void setCampaignCount(long value) {
        this.campaignCount = value;
    }

    @JsonProperty("campaign_last_sent")
    public String getCampaignLastSent() {
        return campaignLastSent;
    }

    @JsonProperty("campaign_last_sent")
    public void setCampaignLastSent(String value) {
        this.campaignLastSent = value;
    }

    @JsonProperty("merge_field_count")
    public long getMergeFieldCount() {
        return mergeFieldCount;
    }

    @JsonProperty("merge_field_count")
    public void setMergeFieldCount(long value) {
        this.mergeFieldCount = value;
    }

    @JsonProperty("avg_sub_rate")
    public long getAvgSubRate() {
        return avgSubRate;
    }

    @JsonProperty("avg_sub_rate")
    public void setAvgSubRate(long value) {
        this.avgSubRate = value;
    }

    @JsonProperty("avg_unsub_rate")
    public long getAvgUnsubRate() {
        return avgUnsubRate;
    }

    @JsonProperty("avg_unsub_rate")
    public void setAvgUnsubRate(long value) {
        this.avgUnsubRate = value;
    }

    @JsonProperty("target_sub_rate")
    public long getTargetSubRate() {
        return targetSubRate;
    }

    @JsonProperty("target_sub_rate")
    public void setTargetSubRate(long value) {
        this.targetSubRate = value;
    }

    @JsonProperty("open_rate")
    public long getOpenRate() {
        return openRate;
    }

    @JsonProperty("open_rate")
    public void setOpenRate(long value) {
        this.openRate = value;
    }

    @JsonProperty("click_rate")
    public long getClickRate() {
        return clickRate;
    }

    @JsonProperty("click_rate")
    public void setClickRate(long value) {
        this.clickRate = value;
    }

    @JsonProperty("last_sub_date")
    public String getLastSubDate() {
        return lastSubDate;
    }

    @JsonProperty("last_sub_date")
    public void setLastSubDate(String value) {
        this.lastSubDate = value;
    }

    @JsonProperty("last_unsub_date")
    public String getLastUnsubDate() {
        return lastUnsubDate;
    }

    @JsonProperty("last_unsub_date")
    public void setLastUnsubDate(String value) {
        this.lastUnsubDate = value;
    }
}
