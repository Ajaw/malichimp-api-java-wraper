package Methods.Lists.Request;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Type {
    @JsonProperty
    regular,
    @JsonProperty
    plaintext,
    @JsonProperty
    absplit,
    @JsonProperty
    rss,
    @JsonProperty
    variate;
}
