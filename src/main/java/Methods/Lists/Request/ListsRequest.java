package Methods.Lists.Request;

import Methods.BaseMalichimpRequest;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ListsRequest extends BaseMalichimpRequest {
    private String name;
    private Contact contact;
    private String permissionReminder;
    private CampaignDefaults campaignDefaults;
    private boolean emailTypeOption;


    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String value) {
        this.name = value;
    }

    @JsonProperty("contact")
    public Contact getContact() {
        return contact;
    }

    @JsonProperty("contact")
    public void setContact(Contact value) {
        this.contact = value;
    }

    @JsonProperty("permission_reminder")
    public String getPermissionReminder() {
        return permissionReminder;
    }

    @JsonProperty("permission_reminder")
    public void setPermissionReminder(String value) {
        this.permissionReminder = value;
    }

    @JsonProperty("campaign_defaults")
    public CampaignDefaults getCampaignDefaults() {
        return campaignDefaults;
    }

    @JsonProperty("campaign_defaults")
    public void setCampaignDefaults(CampaignDefaults value) {
        this.campaignDefaults = value;
    }

    @JsonProperty("email_type_option")
    public boolean getEmailTypeOption() {
        return emailTypeOption;
    }

    @JsonProperty("email_type_option")
    public void setEmailTypeOption(boolean value) {
        this.emailTypeOption = value;
    }
}
