package Methods.Reports.Unsubscribed;

import Client.MailchimpClient;
import Methods.Reports.EmailActivity.Response.EmailActivityResponse;
import Methods.Reports.Unsubscribed.Response.Unsubscribe;
import Methods.Reports.Unsubscribed.Response.UnsubscribeResponse;
import Methods.ServiceMethods;
import exception.RequestFailedException;

public class MailChimpUnsubscribeRequests {


    private MailchimpClient mailchimpClient;

    public MailChimpUnsubscribeRequests(MailchimpClient mailchimpClient) {
        this.mailchimpClient = mailchimpClient;
    }

    private final String campaing="{campaign_id}";
    private final String subsricberHash = "{subscriber_hash}";

    public UnsubscribeResponse getUnsubscribeReport(String camapignId) throws RequestFailedException {
        String method = ServiceMethods.getUnsubscribeReportForCampaing;
        method = method.replace(campaing,camapignId);
        return (UnsubscribeResponse) mailchimpClient.performGetRequest(method, UnsubscribeResponse.class);
    }

    public Unsubscribe getSingleSubscriberUnsubscirbeResponse(String camapignId, String _subsricberHash) throws RequestFailedException {
        String method = ServiceMethods.getUnsubscribeReportForSingleSubscriber;
        method = method.replace(campaing,camapignId);
        method = method.replace(subsricberHash,_subsricberHash);
        return (Unsubscribe) mailchimpClient.performGetRequest(method, Unsubscribe.class);
    }

}
