
package Methods.Reports.Unsubscribed.Response;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Methods.BaseMailchimpResponse;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * Unsubscribes
 * <p>
 * A member who unsubscribed from a specific campaign.
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "email_id",
    "email_address",
    "timestamp",
    "reason",
    "campaign_id",
    "list_id",
    "list_is_active",
    "_links"
})
public class Unsubscribe extends BaseMailchimpResponse {

    /**
     * Email ID
     * <p>
     * The list-specific ID for the given email address
     * 
     */
    @JsonProperty("email_id")
    @JsonPropertyDescription("The list-specific ID for the given email address")
    private String emailId;
    /**
     * Email Address
     * <p>
     * Email address for a subscriber
     * 
     */
    @JsonProperty("email_address")
    @JsonPropertyDescription("Email address for a subscriber")
    private String emailAddress;
    /**
     * Timestamp
     * <p>
     * The date and time the member opted-out in ISO 8601 format.
     * 
     */
    @JsonProperty("timestamp")
    @JsonPropertyDescription("The date and time the member opted-out in ISO 8601 format.")
    private Date timestamp;
    /**
     * Unsusbcribe Reason
     * <p>
     * If available, the reason listed by the member for unsubscribing.
     * 
     */
    @JsonProperty("reason")
    @JsonPropertyDescription("If available, the reason listed by the member for unsubscribing.")
    private String reason;
    /**
     * Campaign ID
     * <p>
     * The id for the campaign.
     * 
     */
    @JsonProperty("campaign_id")
    @JsonPropertyDescription("The id for the campaign.")
    private String campaignId;
    /**
     * List ID
     * <p>
     * The id for the list.
     * 
     */
    @JsonProperty("list_id")
    @JsonPropertyDescription("The id for the list.")
    private String listId;
    /**
     * List Status
     * <p>
     * The status of the list used, namely if it's deleted or disabled.
     * 
     */
    @JsonProperty("list_is_active")
    @JsonPropertyDescription("The status of the list used, namely if it's deleted or disabled.")
    private Boolean listIsActive;
    /**
     * Links
     * <p>
     * 
     * 
     */
    @JsonProperty("_links")
    @JsonPropertyDescription("")
    private List<Link> links = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * Email ID
     * <p>
     * The list-specific ID for the given email address
     * 
     */
    @JsonProperty("email_id")
    public String getEmailId() {
        return emailId;
    }

    /**
     * Email ID
     * <p>
     * The list-specific ID for the given email address
     * 
     */
    @JsonProperty("email_id")
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    /**
     * Email Address
     * <p>
     * Email address for a subscriber
     * 
     */
    @JsonProperty("email_address")
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Email Address
     * <p>
     * Email address for a subscriber
     * 
     */
    @JsonProperty("email_address")
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * Timestamp
     * <p>
     * The date and time the member opted-out in ISO 8601 format.
     * 
     */
    @JsonProperty("timestamp")
    public Date getTimestamp() {
        return timestamp;
    }

    /**
     * Timestamp
     * <p>
     * The date and time the member opted-out in ISO 8601 format.
     * 
     */
    @JsonProperty("timestamp")
    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Unsusbcribe Reason
     * <p>
     * If available, the reason listed by the member for unsubscribing.
     * 
     */
    @JsonProperty("reason")
    public String getReason() {
        return reason;
    }

    /**
     * Unsusbcribe Reason
     * <p>
     * If available, the reason listed by the member for unsubscribing.
     * 
     */
    @JsonProperty("reason")
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * Campaign ID
     * <p>
     * The id for the campaign.
     * 
     */
    @JsonProperty("campaign_id")
    public String getCampaignId() {
        return campaignId;
    }

    /**
     * Campaign ID
     * <p>
     * The id for the campaign.
     * 
     */
    @JsonProperty("campaign_id")
    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    /**
     * List ID
     * <p>
     * The id for the list.
     * 
     */
    @JsonProperty("list_id")
    public String getListId() {
        return listId;
    }

    /**
     * List ID
     * <p>
     * The id for the list.
     * 
     */
    @JsonProperty("list_id")
    public void setListId(String listId) {
        this.listId = listId;
    }

    /**
     * List Status
     * <p>
     * The status of the list used, namely if it's deleted or disabled.
     * 
     */
    @JsonProperty("list_is_active")
    public Boolean getListIsActive() {
        return listIsActive;
    }

    /**
     * List Status
     * <p>
     * The status of the list used, namely if it's deleted or disabled.
     * 
     */
    @JsonProperty("list_is_active")
    public void setListIsActive(Boolean listIsActive) {
        this.listIsActive = listIsActive;
    }

    /**
     * Links
     * <p>
     * 
     * 
     */
    @JsonProperty("_links")
    public List<Link> getLinks() {
        return links;
    }

    /**
     * Links
     * <p>
     * 
     * 
     */
    @JsonProperty("_links")
    public void setLinks(List<Link> links) {
        this.links = links;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
