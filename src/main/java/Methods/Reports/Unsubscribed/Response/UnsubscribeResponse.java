
package Methods.Reports.Unsubscribed.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Methods.BaseMailchimpResponse;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * Unsubscribes
 * <p>
 * A list of members who have unsubscribed from a specific campaign.
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "unsubscribes",
    "campaign_id",
    "_links",
    "total_items"
})
public class UnsubscribeResponse extends BaseMailchimpResponse {

    /**
     * Lists
     * <p>
     * An array of objects, each representing a member who unsubscribed from a campaign.
     * 
     */
    @JsonProperty("unsubscribes")
    @JsonPropertyDescription("An array of objects, each representing a member who unsubscribed from a campaign.")
    private List<Unsubscribe> unsubscribes = null;
    /**
     * Campaign ID
     * <p>
     * The id for the campaign.
     * 
     */
    @JsonProperty("campaign_id")
    @JsonPropertyDescription("The id for the campaign.")
    private String campaignId;
    /**
     * Links
     * <p>
     * 
     * 
     */
    @JsonProperty("_links")
    @JsonPropertyDescription("")
    private List<Link> links = null;
    /**
     * Item Count
     * <p>
     * The total number of items matching the query, irrespective of pagination.
     * 
     */
    @JsonProperty("total_items")
    @JsonPropertyDescription("The total number of items matching the query, irrespective of pagination.")
    private Integer totalItems;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * Lists
     * <p>
     * An array of objects, each representing a member who unsubscribed from a campaign.
     * 
     */
    @JsonProperty("unsubscribes")
    public List<Unsubscribe> getUnsubscribes() {
        return unsubscribes;
    }

    /**
     * Lists
     * <p>
     * An array of objects, each representing a member who unsubscribed from a campaign.
     * 
     */
    @JsonProperty("unsubscribes")
    public void setUnsubscribes(List<Unsubscribe> unsubscribes) {
        this.unsubscribes = unsubscribes;
    }

    /**
     * Campaign ID
     * <p>
     * The id for the campaign.
     * 
     */
    @JsonProperty("campaign_id")
    public String getCampaignId() {
        return campaignId;
    }

    /**
     * Campaign ID
     * <p>
     * The id for the campaign.
     * 
     */
    @JsonProperty("campaign_id")
    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    /**
     * Links
     * <p>
     * 
     * 
     */
    @JsonProperty("_links")
    public List<Link> getLinks() {
        return links;
    }

    /**
     * Links
     * <p>
     * 
     * 
     */
    @JsonProperty("_links")
    public void setLinks(List<Link> links) {
        this.links = links;
    }

    /**
     * Item Count
     * <p>
     * The total number of items matching the query, irrespective of pagination.
     * 
     */
    @JsonProperty("total_items")
    public Integer getTotalItems() {
        return totalItems;
    }

    /**
     * Item Count
     * <p>
     * The total number of items matching the query, irrespective of pagination.
     * 
     */
    @JsonProperty("total_items")
    public void setTotalItems(Integer totalItems) {
        this.totalItems = totalItems;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
