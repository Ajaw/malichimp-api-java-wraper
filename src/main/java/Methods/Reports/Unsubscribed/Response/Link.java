
package Methods.Reports.Unsubscribed.Response;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonValue;


/**
 * Resource Link
 * <p>
 * This object represents a link from the resource where it is found to another resource or action that may be performed.
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "rel",
    "href",
    "method",
    "targetSchema",
    "schema"
})
public class Link {

    /**
     * As with an HTML 'rel' attribute, this describes the type of link.
     * 
     */
    @JsonProperty("rel")
    @JsonPropertyDescription("As with an HTML 'rel' attribute, this describes the type of link.")
    private String rel;
    /**
     * This property contains a fully-qualified URL that can be called to retrieve the linked resource or perform the linked action.
     * 
     */
    @JsonProperty("href")
    @JsonPropertyDescription("This property contains a fully-qualified URL that can be called to retrieve the linked resource or perform the linked action.")
    private String href;
    /**
     * The HTTP method that should be used when accessing the URL defined in 'href'.
     * 
     */
    @JsonProperty("method")
    @JsonPropertyDescription("The HTTP method that should be used when accessing the URL defined in 'href'.")
    private Link.Method method;
    /**
     * For GETs, this is a URL representing the schema that the response should conform to.
     * 
     */
    @JsonProperty("targetSchema")
    @JsonPropertyDescription("For GETs, this is a URL representing the schema that the response should conform to.")
    private String targetSchema;
    /**
     * For HTTP methods that can receive bodies (POST and PUT), this is a URL representing the schema that the body should conform to.
     * 
     */
    @JsonProperty("schema")
    @JsonPropertyDescription("For HTTP methods that can receive bodies (POST and PUT), this is a URL representing the schema that the body should conform to.")
    private String schema;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * As with an HTML 'rel' attribute, this describes the type of link.
     * 
     */
    @JsonProperty("rel")
    public String getRel() {
        return rel;
    }

    /**
     * As with an HTML 'rel' attribute, this describes the type of link.
     * 
     */
    @JsonProperty("rel")
    public void setRel(String rel) {
        this.rel = rel;
    }

    /**
     * This property contains a fully-qualified URL that can be called to retrieve the linked resource or perform the linked action.
     * 
     */
    @JsonProperty("href")
    public String getHref() {
        return href;
    }

    /**
     * This property contains a fully-qualified URL that can be called to retrieve the linked resource or perform the linked action.
     * 
     */
    @JsonProperty("href")
    public void setHref(String href) {
        this.href = href;
    }

    /**
     * The HTTP method that should be used when accessing the URL defined in 'href'.
     * 
     */
    @JsonProperty("method")
    public Link.Method getMethod() {
        return method;
    }

    /**
     * The HTTP method that should be used when accessing the URL defined in 'href'.
     * 
     */
    @JsonProperty("method")
    public void setMethod(Link.Method method) {
        this.method = method;
    }

    /**
     * For GETs, this is a URL representing the schema that the response should conform to.
     * 
     */
    @JsonProperty("targetSchema")
    public String getTargetSchema() {
        return targetSchema;
    }

    /**
     * For GETs, this is a URL representing the schema that the response should conform to.
     * 
     */
    @JsonProperty("targetSchema")
    public void setTargetSchema(String targetSchema) {
        this.targetSchema = targetSchema;
    }

    /**
     * For HTTP methods that can receive bodies (POST and PUT), this is a URL representing the schema that the body should conform to.
     * 
     */
    @JsonProperty("schema")
    public String getSchema() {
        return schema;
    }

    /**
     * For HTTP methods that can receive bodies (POST and PUT), this is a URL representing the schema that the body should conform to.
     * 
     */
    @JsonProperty("schema")
    public void setSchema(String schema) {
        this.schema = schema;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public enum Method {

        GET("GET"),
        POST("POST"),
        PUT("PUT"),
        PATCH("PATCH"),
        DELETE("DELETE"),
        OPTIONS("OPTIONS"),
        HEAD("HEAD");
        private final String value;
        private final static Map<String, Link.Method> CONSTANTS = new HashMap<String, Link.Method>();

        static {
            for (Link.Method c: values()) {
                CONSTANTS.put(c.value, c);
            }
        }

        private Method(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

        @JsonValue
        public String value() {
            return this.value;
        }

        @JsonCreator
        public static Link.Method fromValue(String value) {
            Link.Method constant = CONSTANTS.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }

}
