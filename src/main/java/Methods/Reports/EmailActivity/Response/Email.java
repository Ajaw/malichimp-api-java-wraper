
package Methods.Reports.EmailActivity.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Methods.BaseMailchimpResponse;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * Email Activity
 * <p>
 * A list of a member's subscriber activity in a specific campaign, including opens, clicks, and bounces.
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "campaign_id",
    "list_id",
    "list_is_active",
    "email_id",
    "email_address",
    "activity",
    "_links"
})
public class Email extends BaseMailchimpResponse {

    /**
     * Campaign ID
     * <p>
     * The unique id for the campaign.
     * 
     */
    @JsonProperty("campaign_id")
    @JsonPropertyDescription("The unique id for the campaign.")
    private String campaignId;
    /**
     * List ID
     * <p>
     * The unique id for the list.
     * 
     */
    @JsonProperty("list_id")
    @JsonPropertyDescription("The unique id for the list.")
    private String listId;
    /**
     * List Status
     * <p>
     * The status of the list used, namely if it's deleted or disabled.
     * 
     */
    @JsonProperty("list_is_active")
    @JsonPropertyDescription("The status of the list used, namely if it's deleted or disabled.")
    private Boolean listIsActive;
    /**
     * Email Hash
     * <p>
     * The MD5 hash of the lowercase version of the list member's email address.
     * 
     */
    @JsonProperty("email_id")
    @JsonPropertyDescription("The MD5 hash of the lowercase version of the list member's email address.")
    private String emailId;
    /**
     * Email Address
     * <p>
     * Email address for a subscriber.
     * 
     */
    @JsonProperty("email_address")
    @JsonPropertyDescription("Email address for a subscriber.")
    private String emailAddress;
    /**
     * Member Activity
     * <p>
     * An array of objects, each showing an interaction with the email.
     * 
     */
    @JsonProperty("activity")
    @JsonPropertyDescription("An array of objects, each showing an interaction with the email.")
    private List<Activity> activity = null;
    /**
     * Links
     * <p>
     * A list of link types and descriptions for the API schema documents.
     * 
     */
    @JsonProperty("_links")
    @JsonPropertyDescription("A list of link types and descriptions for the API schema documents.")
    private List<Link> links = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * Campaign ID
     * <p>
     * The unique id for the campaign.
     * 
     */
    @JsonProperty("campaign_id")
    public String getCampaignId() {
        return campaignId;
    }

    /**
     * Campaign ID
     * <p>
     * The unique id for the campaign.
     * 
     */
    @JsonProperty("campaign_id")
    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    /**
     * List ID
     * <p>
     * The unique id for the list.
     * 
     */
    @JsonProperty("list_id")
    public String getListId() {
        return listId;
    }

    /**
     * List ID
     * <p>
     * The unique id for the list.
     * 
     */
    @JsonProperty("list_id")
    public void setListId(String listId) {
        this.listId = listId;
    }

    /**
     * List Status
     * <p>
     * The status of the list used, namely if it's deleted or disabled.
     * 
     */
    @JsonProperty("list_is_active")
    public Boolean getListIsActive() {
        return listIsActive;
    }

    /**
     * List Status
     * <p>
     * The status of the list used, namely if it's deleted or disabled.
     * 
     */
    @JsonProperty("list_is_active")
    public void setListIsActive(Boolean listIsActive) {
        this.listIsActive = listIsActive;
    }

    /**
     * Email Hash
     * <p>
     * The MD5 hash of the lowercase version of the list member's email address.
     * 
     */
    @JsonProperty("email_id")
    public String getEmailId() {
        return emailId;
    }

    /**
     * Email Hash
     * <p>
     * The MD5 hash of the lowercase version of the list member's email address.
     * 
     */
    @JsonProperty("email_id")
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    /**
     * Email Address
     * <p>
     * Email address for a subscriber.
     * 
     */
    @JsonProperty("email_address")
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Email Address
     * <p>
     * Email address for a subscriber.
     * 
     */
    @JsonProperty("email_address")
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * Member Activity
     * <p>
     * An array of objects, each showing an interaction with the email.
     * 
     */
    @JsonProperty("activity")
    public List<Activity> getActivity() {
        return activity;
    }

    /**
     * Member Activity
     * <p>
     * An array of objects, each showing an interaction with the email.
     * 
     */
    @JsonProperty("activity")
    public void setActivity(List<Activity> activity) {
        this.activity = activity;
    }

    /**
     * Links
     * <p>
     * A list of link types and descriptions for the API schema documents.
     * 
     */
    @JsonProperty("_links")
    public List<Link> getLinks() {
        return links;
    }

    /**
     * Links
     * <p>
     * A list of link types and descriptions for the API schema documents.
     * 
     */
    @JsonProperty("_links")
    public void setLinks(List<Link> links) {
        this.links = links;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
