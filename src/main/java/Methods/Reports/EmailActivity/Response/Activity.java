
package Methods.Reports.EmailActivity.Response;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * Member Activity
 * <p>
 * A summary of the interaction with the campaign.
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "action",
    "type",
    "timestamp",
    "url",
    "ip"
})
public class Activity {

    /**
     * Action
     * <p>
     * One of the following actions: 'open', 'click', or 'bounce'
     * 
     */
    @JsonProperty("action")
    @JsonPropertyDescription("One of the following actions: 'open', 'click', or 'bounce'")
    private String action;
    /**
     * Type
     * <p>
     * If the action is a 'bounce', the type of bounce received: 'hard', 'soft'.
     * 
     */
    @JsonProperty("type")
    @JsonPropertyDescription("If the action is a 'bounce', the type of bounce received: 'hard', 'soft'.")
    private String type;
    /**
     * Timestamp
     * <p>
     * The date and time recorded for the action in ISO 8601 format.
     * 
     */
    @JsonProperty("timestamp")
    @JsonPropertyDescription("The date and time recorded for the action in ISO 8601 format.")
    private Date timestamp;
    /**
     * URL
     * <p>
     * If the action is a 'click', the URL on which the member clicked.
     * 
     */
    @JsonProperty("url")
    @JsonPropertyDescription("If the action is a 'click', the URL on which the member clicked.")
    private String url;
    /**
     * IP
     * <p>
     * The IP address recorded for the action.
     * 
     */
    @JsonProperty("ip")
    @JsonPropertyDescription("The IP address recorded for the action.")
    private String ip;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * Action
     * <p>
     * One of the following actions: 'open', 'click', or 'bounce'
     * 
     */
    @JsonProperty("action")
    public String getAction() {
        return action;
    }

    /**
     * Action
     * <p>
     * One of the following actions: 'open', 'click', or 'bounce'
     * 
     */
    @JsonProperty("action")
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * Type
     * <p>
     * If the action is a 'bounce', the type of bounce received: 'hard', 'soft'.
     * 
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     * Type
     * <p>
     * If the action is a 'bounce', the type of bounce received: 'hard', 'soft'.
     * 
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Timestamp
     * <p>
     * The date and time recorded for the action in ISO 8601 format.
     * 
     */
    @JsonProperty("timestamp")
    public Date getTimestamp() {
        return timestamp;
    }

    /**
     * Timestamp
     * <p>
     * The date and time recorded for the action in ISO 8601 format.
     * 
     */
    @JsonProperty("timestamp")
    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * URL
     * <p>
     * If the action is a 'click', the URL on which the member clicked.
     * 
     */
    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    /**
     * URL
     * <p>
     * If the action is a 'click', the URL on which the member clicked.
     * 
     */
    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * IP
     * <p>
     * The IP address recorded for the action.
     * 
     */
    @JsonProperty("ip")
    public String getIp() {
        return ip;
    }

    /**
     * IP
     * <p>
     * The IP address recorded for the action.
     * 
     */
    @JsonProperty("ip")
    public void setIp(String ip) {
        this.ip = ip;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
