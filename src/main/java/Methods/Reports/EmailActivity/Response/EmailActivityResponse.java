
package Methods.Reports.EmailActivity.Response;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Methods.BaseMailchimpResponse;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * Email Activity
 * <p>
 * A list of member's subscriber activity in a specific campaign.
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "emails",
    "campaign_id",
    "total_items",
    "_links"
})
public class EmailActivityResponse extends BaseMailchimpResponse {

    /**
     * Sent To
     * <p>
     * An array of objects, each representing a member who opened a campaign.
     * 
     */
    @JsonProperty("emails")
    @JsonPropertyDescription("An array of objects, each representing a member who opened a campaign.")
    private List<Email> emails = null;
    /**
     * Campaign ID
     * <p>
     * The unique id for the sent campaign.
     * 
     */
    @JsonProperty("campaign_id")
    @JsonPropertyDescription("The unique id for the sent campaign.")
    private String campaignId;
    /**
     * Item Count
     * <p>
     * The total number of items matching the query regardless of pagination.
     * 
     */
    @JsonProperty("total_items")
    @JsonPropertyDescription("The total number of items matching the query regardless of pagination.")
    private Integer totalItems;
    /**
     * Links
     * <p>
     * A list of link types and descriptions for the API schema documents.
     * 
     */
    @JsonProperty("_links")
    @JsonPropertyDescription("A list of link types and descriptions for the API schema documents.")
    private List<Link> links = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * Sent To
     * <p>
     * An array of objects, each representing a member who opened a campaign.
     * 
     */
    @JsonProperty("emails")
    public List<Email> getEmails() {
        return emails;
    }

    /**
     * Sent To
     * <p>
     * An array of objects, each representing a member who opened a campaign.
     * 
     */
    @JsonProperty("emails")
    public void setEmails(List<Email> emails) {
        this.emails = emails;
    }

    /**
     * Campaign ID
     * <p>
     * The unique id for the sent campaign.
     * 
     */
    @JsonProperty("campaign_id")
    public String getCampaignId() {
        return campaignId;
    }

    /**
     * Campaign ID
     * <p>
     * The unique id for the sent campaign.
     * 
     */
    @JsonProperty("campaign_id")
    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    /**
     * Item Count
     * <p>
     * The total number of items matching the query regardless of pagination.
     * 
     */
    @JsonProperty("total_items")
    public Integer getTotalItems() {
        return totalItems;
    }

    /**
     * Item Count
     * <p>
     * The total number of items matching the query regardless of pagination.
     * 
     */
    @JsonProperty("total_items")
    public void setTotalItems(Integer totalItems) {
        this.totalItems = totalItems;
    }

    /**
     * Links
     * <p>
     * A list of link types and descriptions for the API schema documents.
     * 
     */
    @JsonProperty("_links")
    public List<Link> getLinks() {
        return links;
    }

    /**
     * Links
     * <p>
     * A list of link types and descriptions for the API schema documents.
     * 
     */
    @JsonProperty("_links")
    public void setLinks(List<Link> links) {
        this.links = links;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
