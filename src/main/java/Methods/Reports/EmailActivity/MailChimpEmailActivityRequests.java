package Methods.Reports.EmailActivity;

import Client.MailchimpClient;
import Methods.Lists.Request.ListsRequest;
import Methods.Lists.Response.List;
import Methods.Lists.Response.ListsResponse;
import Methods.Reports.EmailActivity.Response.Email;
import Methods.Reports.EmailActivity.Response.EmailActivityResponse;
import Methods.ServiceMethods;
import exception.RequestFailedException;

public class MailChimpEmailActivityRequests {


    private MailchimpClient mailchimpClient;

    public MailChimpEmailActivityRequests(MailchimpClient mailchimpClient) {
        this.mailchimpClient = mailchimpClient;
    }

    private final String campaing="{campaign_id}";
    private final String subsricberHash = "{subscriber_hash}";

    public EmailActivityResponse getEmailActivityResponse(String camapignId) throws RequestFailedException {
        String method = ServiceMethods.getEmailActivityReportForCampaing;
        method = method.replace(campaing,camapignId);
        return (EmailActivityResponse) mailchimpClient.performGetRequest(method, EmailActivityResponse.class);
    }

    public Email getSingleSubscriberEmailActivityResponse(String camapignId, String _subsricberHash) throws RequestFailedException {
        String method = ServiceMethods.getEmailActivityReportForSingleSubscriber;
        method = method.replace(camapignId,camapignId);
        method = method.replace(campaing,_subsricberHash);
        return (Email) mailchimpClient.performGetRequest(method, Email.class);
    }

}
